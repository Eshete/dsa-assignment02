import wso2/kafka;
 
kafka:ProducerConfig producerConfigs = {
    //producer with parameters.
    bootstrapServers: "localhost:9092",
    clientId:"dist",
    acks:"all",
    noRetries:5
};
 
kafka:Producer kafkaProducer = new(producerConfigs);
 
function main () {
    string sq = "Sending notification to all remote servers";
    byte[] serializedMsg = sq.toByteArray("UTF-8");
    var sendResult = kafkaProducer-&gt;send(serializedMsg, "DFS");
    if (sendResult is error) {
        log:printError("We could not update remote servers", err = sendResult);
    }
}
 